# shell.nix
let 
  pkgs = import <nixpkgs> { };
#  compiler = "ghc92";
in 
  pkgs.haskellPackages.developPackage {
    root = ./.;
    source-overrides = {
      datetime = builtins.fetchGit {
        url = "https://gitlab.haskell.org/fstuess/datetime.git";
	ref = "master";
	rev = "23f717058df869488799aa03894ce4e9ad17b8f8";
      };
      serialport = builtins.fetchGit {
	url = "http://funk1:3000/funk/serialport.git";
        ref = "master";
        rev = "ea44dceb57053c2ee4039c6ff9b2a67f491f4b60";
      };
    };
    modifier = drv:
      pkgs.haskell.lib.addBuildTools drv (with pkgs.haskellPackages;
        [ cabal-install
          cabal-fmt
          cabal2nix
          ghcid
          hlint
          haskell-language-server
          pkgs.vscode
        ]);
  }
